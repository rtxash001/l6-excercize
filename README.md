# L6 Excercize

Description HAT


This would be a Raspberry Pi DC motor driver board with an on-board motor controller, that keeps track of the position, speed and direction of the motor. The most precise position sensor to achieve required accuracy is the optical encoder which is composed of an LED light source, masked disc attached to the motor shaft and photodetector. Alternatively, motors which include Hall effect sensors in their construction could be coupled with an Op Amp Differential amplifier to provide an analog signal within 0-3.3V that suits the Pi input voltage. The HAT would also include a motor driver IC, power supply unit for the DC motor which and the circuitry needed to monitor the motor position providing that input signal within the required voltage range. 
